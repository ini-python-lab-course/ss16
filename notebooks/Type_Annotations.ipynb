{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Type Annotations in Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With Python 3.5 type annotations were famously (and controversely) introduced to the language (PEP 484).\n",
    "For a full documentation see https://www.python.org/dev/peps/pep-0484.\n",
    "Especially interesting for programming in Python 2.7 is section https://www.python.org/dev/peps/pep-0484/#suggested-syntax-for-python-2-7-and-straddling-code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Why typechecking?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that Python is not a type-free language; it rather is implicitly typed. While types are not explicitly annotated (until now!), calls can go horribly wrong if you use variables of wrong type. So type annotations are in first place an important part of a function's or method's usage documentation. There were frequent approaches to come up with a structured documentation syntax for type information. Most famously Sphinx-autodoc, NumPy and Google came up with normalized formats for this. While the Sphinx approach is rather markup-oriented the other solutions yield much better readable docstrings if you look at them in plain-text. See http://www.sphinx-doc.org/en/stable/ext/autodoc.html for further reading and type annotation docstrings section further below.\n",
    "\n",
    "Besides documentation purposes there are static (i.e. pre-runtime) type checkers available:\n",
    "\n",
    "http://mypy-lang.org\n",
    "\n",
    "https://github.com/google/pytype\n",
    "\n",
    "Static typechecking can improve code consistency a lot. Using unit tests for this purpose can only cover a limited sample of use-cases.\n",
    "\n",
    "While often frowned upon type annotations can also be used for runtime-typechecking:\n",
    "\n",
    "https://github.com/prechelt/typecheck-decorator (Python3 only)\n",
    "\n",
    "Checking for correct types has been a frequent usecase of unittests (see the unittest notebook) and runtime assertions, which are both essential concepts for debugging in Python."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Intermezzo: Runtime assertions\n",
    "\n",
    "Using the assert-statement one can confirm that things are like expected:\n",
    "\n",
    "https://wiki.python.org/moin/UsingAssertionsEffectively\n",
    "\n",
    "A typical Python \"feature\" is that an ill-formed call can trigger around for some time before the issue becomes visible. The assert statement can help to let blow things up near the actual cause of the issue."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5.0"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "def pythagoras_hypothenuse(a, b):\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3, 4)\n",
    "pythagoras_hypothenuse(3, -4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We might more explicitly want to ensure that this is not called with strings or so."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "def pythagoras_hypothenuse(a, b):\n",
    "    assert type(a) == int or type(a) == float\n",
    "    assert type(b) == int or type(b) == float\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3.6, \"4\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Checking every number type on its own is unhandy though. So a better variant is like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5.3814496188294845"
      ]
     },
     "execution_count": 16,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from numbers import Real\n",
    "\n",
    "def pythagoras_hypothenuse(a, b):\n",
    "    assert isinstance(b, Real)\n",
    "    assert isinstance(b, Real)\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3.6, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Type Annotations\n",
    "Note that type annotations as presented in this section are so far only a normalized notation. You will need to apply an additional tool to get actual benefit (other than code documentation).\n",
    "Using a typechecker-decorator you can achieve something similar to the assert statement as far as types are concerned, but in a far more compact and readable notation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python 3.5"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "5.3814496188294845"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from numbers import Real\n",
    "\n",
    "def pythagoras_hypothenuse(a: Real, b: Real) -> float:\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3.6, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Python 2.7"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from numbers import Real\n",
    "\n",
    "def pythagoras_hypothenuse(a, b):\n",
    "    # type: (Real, Real) -> float\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3.6, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively annotate one parameter per line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from numbers import Real\n",
    "\n",
    "def pythagoras_hypothenuse(\n",
    "                        a, # type: Real\n",
    "                        b  # type: Real\n",
    "                            ):\n",
    "    # type: (...) -> float\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(3.6, 4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Runtime typechecking\n",
    "Get a Python 2.7-compliant runtime typechecker via\n",
    "\n",
    "```\n",
    "git clone https://github.com/Stewori/pytypes\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "InputTypeError",
     "evalue": "__main__.pythagoras_hypothenuse called with incompatible types:\nExpected: typing.Tuple[numbers.Real, numbers.Real]\nGot:      typing.Tuple[int, str]",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mInputTypeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-7-57bca4140f43>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[0;32m     12\u001b[0m     \u001b[1;32mreturn\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0ma\u001b[0m\u001b[1;33m**\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m+\u001b[0m\u001b[0mb\u001b[0m\u001b[1;33m**\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m**\u001b[0m\u001b[1;36m0.5\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m     13\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 14\u001b[1;33m \u001b[0mpythagoras_hypothenuse\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m7\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m'3'\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36mchecker_tp\u001b[1;34m(*args, **kw)\u001b[0m\n\u001b[0;32m    320\u001b[0m                 \u001b[0mresSigs\u001b[0m \u001b[1;33m=\u001b[0m \u001b[1;33m[\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    321\u001b[0m                 \u001b[1;32mfor\u001b[0m \u001b[0mffunc\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mtoCheck\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 322\u001b[1;33m                         \u001b[0mresSigs\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mappend\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0m_checkfunctype\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtp\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mffunc\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mslf\u001b[0m \u001b[1;32mor\u001b[0m \u001b[0mclsm\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0margs\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;36m0\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__class__\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m    323\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    324\u001b[0m                 \u001b[1;31m# perform backend-call:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36m_checkfunctype\u001b[1;34m(tp, func, slf, func_class)\u001b[0m\n\u001b[0;32m    241\u001b[0m                                         \u001b[1;33m%\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__module__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    242\u001b[0m                                         \u001b[1;33m+\u001b[0m \u001b[1;34m\"Expected: %s\\nGot:      %s\"\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 243\u001b[1;33m \t\t\t\t\t% (str(argSig), str(tp)))\n\u001b[0m\u001b[0;32m    244\u001b[0m         \u001b[1;32mreturn\u001b[0m \u001b[0mresSig\u001b[0m \u001b[1;31m# provide this by-product for potential future use\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    245\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mInputTypeError\u001b[0m: __main__.pythagoras_hypothenuse called with incompatible types:\nExpected: typing.Tuple[numbers.Real, numbers.Real]\nGot:      typing.Tuple[int, str]"
     ]
    }
   ],
   "source": [
    "import sys\n",
    "sys.path.append(\"../../pytypes/src\")\n",
    "#sys.path.append(\"C:/Users/vpyt0003/pytypes/src\")\n",
    "from typechecker import *\n",
    "from numbers import Real\n",
    "\n",
    "@typechecked\n",
    "def pythagoras_hypothenuse(a, b):\n",
    "    # type: (Real, Real) -> float\n",
    "    assert a >= 0\n",
    "    assert b >= 0\n",
    "    return (a**2+b**2)**0.5\n",
    "\n",
    "pythagoras_hypothenuse(7, '3')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "class SomeClass():\n",
    "    def pythagoras_hypothenuse(self, a, b):\n",
    "        # type: (Real, Real) -> float\n",
    "        # Don't type-annotate self!\n",
    "        pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "OverrideError",
     "evalue": "__main__.SomeSubclass1.pythagoras_hypothenuse_2 does not override any other method.\n",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mOverrideError\u001b[0m                             Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-24-8702b0064512>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      8\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      9\u001b[0m \u001b[0mssc\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mSomeSubclass1\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 10\u001b[1;33m \u001b[0mssc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mpythagoras_hypothenuse_2\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;36m7\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36mchecker_ov\u001b[1;34m(*args, **kw)\u001b[0m\n\u001b[0;32m    174\u001b[0m                                 \u001b[1;32mif\u001b[0m \u001b[0mlen\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0movmro\u001b[0m\u001b[1;33m)\u001b[0m \u001b[1;33m==\u001b[0m \u001b[1;36m0\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    175\u001b[0m \t\t\t\t\traise OverrideError(\"%s.%s.%s does not override any other method.\\n\"\n\u001b[1;32m--> 176\u001b[1;33m \t\t\t\t\t\t\t% (func.__module__, args[0].__class__.__name__, func.__name__))\n\u001b[0m\u001b[0;32m    177\u001b[0m                                 \u001b[1;31m# Not yet support overloading\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    178\u001b[0m                                 \u001b[1;31m# Check arg-count compatibility\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;31mOverrideError\u001b[0m: __main__.SomeSubclass1.pythagoras_hypothenuse_2 does not override any other method.\n"
     ]
    }
   ],
   "source": [
    "class SomeSubclass1(SomeClass):\n",
    "    @override\n",
    "    def pythagoras_hypothenuse_2(self, a, b):\n",
    "        # type: (str, Real) -> float\n",
    "        assert a >= 0\n",
    "        assert b >= 0\n",
    "        return (a**2+b**2)**0.5\n",
    "\n",
    "ssc = SomeSubclass1()\n",
    "ssc.pythagoras_hypothenuse_2(2, 7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [
    {
     "ename": "OverrideError",
     "evalue": "__main__.SomeSubclass2.pythagoras_hypothenuse cannot override __main__.SomeClass.pythagoras_hypothenuse.\nIncompatible argument types: typing.Tuple[numbers.Real, numbers.Real] is not a subtype of typing.Tuple[str, numbers.Real].",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mOverrideError\u001b[0m                             Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-25-15a640913d0e>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      8\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      9\u001b[0m \u001b[0mssc\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mSomeSubclass2\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 10\u001b[1;33m \u001b[0mssc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mpythagoras_hypothenuse\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;36m7\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36mchecker_ov\u001b[1;34m(*args, **kw)\u001b[0m\n\u001b[0;32m    198\u001b[0m                                                                                 \u001b[1;33m%\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__module__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0margs\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;36m0\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__class__\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0movf\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__module__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0movcls\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0movf\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    199\u001b[0m                                                                                 \u001b[1;33m+\u001b[0m \u001b[1;34m\"Incompatible argument types: %s is not a subtype of %s.\"\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 200\u001b[1;33m \t\t\t\t\t\t\t\t\t\t% (str(ovSig), str(argSig)))\n\u001b[0m\u001b[0;32m    201\u001b[0m                                                         \u001b[1;32mif\u001b[0m \u001b[1;32mnot\u001b[0m \u001b[0missubclass\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mresSig\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0movResSig\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    202\u001b[0m \t\t\t\t\t\t\t\traise OverrideError(\"%s.%s.%s cannot override %s.%s.%s.\\n\"\n",
      "\u001b[1;31mOverrideError\u001b[0m: __main__.SomeSubclass2.pythagoras_hypothenuse cannot override __main__.SomeClass.pythagoras_hypothenuse.\nIncompatible argument types: typing.Tuple[numbers.Real, numbers.Real] is not a subtype of typing.Tuple[str, numbers.Real]."
     ]
    }
   ],
   "source": [
    "class SomeSubclass2(SomeClass):\n",
    "    @override\n",
    "    def pythagoras_hypothenuse(self, a, b):\n",
    "        # type: (str, Real) -> float\n",
    "        assert a >= 0\n",
    "        assert b >= 0\n",
    "        return (a**2+b**2)**0.5\n",
    "\n",
    "ssc = SomeSubclass2()\n",
    "ssc.pythagoras_hypothenuse(2, 7)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 31,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "InputTypeError",
     "evalue": "classmethod __main__.SomeSubclass3.pythagoras_hypothenuse called with incompatible types:\nExpected: typing.Tuple[numbers.Real, numbers.Real]\nGot:      typing.Tuple[int, str]",
     "output_type": "error",
     "traceback": [
      "\u001b[1;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[1;31mInputTypeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[1;32m<ipython-input-31-6a8a9478ff30>\u001b[0m in \u001b[0;36m<module>\u001b[1;34m()\u001b[0m\n\u001b[0;32m      8\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m      9\u001b[0m \u001b[0mssc\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mSomeSubclass3\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m---> 10\u001b[1;33m \u001b[0mssc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mpythagoras_hypothenuse\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;36m2\u001b[0m\u001b[1;33m,\u001b[0m \u001b[1;34m'7'\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36mchecker_tp\u001b[1;34m(*args, **kw)\u001b[0m\n\u001b[0;32m    320\u001b[0m                 \u001b[0mresSigs\u001b[0m \u001b[1;33m=\u001b[0m \u001b[1;33m[\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    321\u001b[0m                 \u001b[1;32mfor\u001b[0m \u001b[0mffunc\u001b[0m \u001b[1;32min\u001b[0m \u001b[0mtoCheck\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 322\u001b[1;33m                         \u001b[0mresSigs\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mappend\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0m_checkfunctype\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0mtp\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mffunc\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mslf\u001b[0m \u001b[1;32mor\u001b[0m \u001b[0mclsm\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0margs\u001b[0m\u001b[1;33m[\u001b[0m\u001b[1;36m0\u001b[0m\u001b[1;33m]\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__class__\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[0;32m    323\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    324\u001b[0m                 \u001b[1;31m# perform backend-call:\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[1;32mC:\\Users\\vpyt0003\\pytypes\\src\\typechecker.py\u001b[0m in \u001b[0;36m_checkfunctype\u001b[1;34m(tp, func, slf, func_class)\u001b[0m\n\u001b[0;32m    236\u001b[0m                                         \u001b[1;33m%\u001b[0m \u001b[1;33m(\u001b[0m\u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__module__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mfunc_class\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m,\u001b[0m \u001b[0mfunc\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m__name__\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    237\u001b[0m                                         \u001b[1;33m+\u001b[0m \u001b[1;34m\"Expected: %s\\nGot:      %s\"\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m--> 238\u001b[1;33m \t\t\t\t\t% (str(argSig), str(tp)))\n\u001b[0m\u001b[0;32m    239\u001b[0m                 \u001b[1;32melse\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m    240\u001b[0m \t\t\traise InputTypeError(\"%s.%s called with incompatible types:\\n\"\n",
      "\u001b[1;31mInputTypeError\u001b[0m: classmethod __main__.SomeSubclass3.pythagoras_hypothenuse called with incompatible types:\nExpected: typing.Tuple[numbers.Real, numbers.Real]\nGot:      typing.Tuple[int, str]"
     ]
    }
   ],
   "source": [
    "class SomeSubclass3(SomeClass):\n",
    "    @typechecked\n",
    "    @override\n",
    "    def pythagoras_hypothenuse(self, a, b):\n",
    "        assert a >= 0\n",
    "        assert b >= 0\n",
    "        return (a**2+b**2)**0.5\n",
    "\n",
    "ssc = SomeSubclass3()\n",
    "ssc.pythagoras_hypothenuse(2, '7')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Type Annotations in docstrings\n",
    "For the latter examples in this section Sphinx with the napoleon-extension is required:\n",
    "http://www.sphinx-doc.org/en/stable/ext/napoleon.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sphinx-autodoc\n",
    "See http://www.sphinx-doc.org/en/stable/ext/autodoc.html for official documentation.\n",
    "With the correct configuration-bits Sphinx will automatically generate nice-looking API-documentation from this sort of docstring:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def function1(self, arg1, arg2, arg3):\n",
    "    \"\"\"\n",
    "    Write some general documentation about the method here.\n",
    "    Below you can add formalized parameter specifications\n",
    "    and other special sections (e.g. 'Example')\n",
    "\n",
    "    :param arg1: the first value\n",
    "    :param arg2: the first value\n",
    "    :param arg3: the first value\n",
    "    :type arg1: int, float,...\n",
    "    :type arg2: int, float,...\n",
    "    :type arg3: int, float,...\n",
    "    :returns: arg1/arg2 +arg3\n",
    "    :rtype: int, float\n",
    "\n",
    "    :Example:\n",
    "\n",
    "    >>> import template\n",
    "    >>> a = template.MainClass1()\n",
    "    >>> a.function1(1,1,1)\n",
    "    2\n",
    "\n",
    "    .. note:: can be useful to emphasize\n",
    "        important feature\n",
    "    .. seealso:: :class:`MainClass2`\n",
    "    .. warning:: arg2 must be non-zero.\n",
    "    .. todo:: check that arg2 is non zero.\n",
    "    \"\"\"\n",
    "    return arg1/arg2 + arg3\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### NumPy-style\n",
    "For a full example see http://www.sphinx-doc.org/en/stable/ext/example_numpy.html. Note that the format to describe types here also originates from Python's official type-annotation format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def module_level_function(param1, param2=None, *args, **kwargs):\n",
    "    \"\"\"This is an example of a module level function.\n",
    "\n",
    "    Function parameters should be documented in the ``Parameters`` section.\n",
    "    The name of each parameter is required. The type and description of each\n",
    "    parameter is optional, but should be included if not obvious.\n",
    "\n",
    "    Parameter types -- if given -- should be specified according to\n",
    "    `PEP 484`_, though `PEP 484`_ conformance isn't required or enforced.\n",
    "\n",
    "    If \\*args or \\*\\*kwargs are accepted,\n",
    "    they should be listed as ``*args`` and ``**kwargs``.\n",
    "\n",
    "    The format for a parameter is::\n",
    "\n",
    "        name : type\n",
    "            description\n",
    "\n",
    "            The description may span multiple lines. Following lines\n",
    "            should be indented to match the first line of the description.\n",
    "            The \": type\" is optional.\n",
    "\n",
    "            Multiple paragraphs are supported in parameter\n",
    "            descriptions.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    param1 : int\n",
    "        The first parameter.\n",
    "    param2 : Optional[str]\n",
    "        The second parameter.\n",
    "    *args\n",
    "        Variable length argument list.\n",
    "    **kwargs\n",
    "        Arbitrary keyword arguments.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    bool\n",
    "        True if successful, False otherwise.\n",
    "\n",
    "        The return type is not optional. The ``Returns`` section may span\n",
    "        multiple lines and paragraphs. Following lines should be indented to\n",
    "        match the first line of the description.\n",
    "\n",
    "        The ``Returns`` section supports any reStructuredText formatting,\n",
    "        including literal blocks::\n",
    "\n",
    "            {\n",
    "                'param1': param1,\n",
    "                'param2': param2\n",
    "            }\n",
    "\n",
    "    Raises\n",
    "    ------\n",
    "    AttributeError\n",
    "        The ``Raises`` section is a list of all exceptions\n",
    "        that are relevant to the interface.\n",
    "    ValueError\n",
    "        If `param2` is equal to `param1`.\n",
    "\n",
    "\n",
    "    .. _PEP 484:\n",
    "       https://www.python.org/dev/peps/pep-0484/\n",
    "\n",
    "    \"\"\"\n",
    "    if param1 == param2:\n",
    "        raise ValueError('param1 may not be equal to param2')\n",
    "    return True\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Google-style\n",
    "For a full example see http://www.sphinx-doc.org/en/stable/ext/example_google.html.\n",
    "Note that the format to describe types here also originates from Python's official type-annotation format."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def module_level_function(param1, param2=None, *args, **kwargs):\n",
    "    \"\"\"This is an example of a module level function.\n",
    "\n",
    "    Function parameters should be documented in the ``Args`` section. The name\n",
    "    of each parameter is required. The type and description of each parameter\n",
    "    is optional, but should be included if not obvious.\n",
    "\n",
    "    Parameter types -- if given -- should be specified according to\n",
    "    `PEP 484`_, though `PEP 484`_ conformance isn't required or enforced.\n",
    "\n",
    "    If \\*args or \\*\\*kwargs are accepted,\n",
    "    they should be listed as ``*args`` and ``**kwargs``.\n",
    "\n",
    "    The format for a parameter is::\n",
    "\n",
    "        name (type): description\n",
    "            The description may span multiple lines. Following\n",
    "            lines should be indented. The \"(type)\" is optional.\n",
    "\n",
    "            Multiple paragraphs are supported in parameter\n",
    "            descriptions.\n",
    "\n",
    "    Args:\n",
    "        param1 (int): The first parameter.\n",
    "        param2 (Optional[str]): The second parameter. Defaults to None.\n",
    "            Second line of description should be indented.\n",
    "        *args: Variable length argument list.\n",
    "        **kwargs: Arbitrary keyword arguments.\n",
    "\n",
    "    Returns:\n",
    "        bool: True if successful, False otherwise.\n",
    "\n",
    "        The return type is optional and may be specified at the beginning of\n",
    "        the ``Returns`` section followed by a colon.\n",
    "\n",
    "        The ``Returns`` section may span multiple lines and paragraphs.\n",
    "        Following lines should be indented to match the first line.\n",
    "\n",
    "        The ``Returns`` section supports any reStructuredText formatting,\n",
    "        including literal blocks::\n",
    "\n",
    "            {\n",
    "                'param1': param1,\n",
    "                'param2': param2\n",
    "            }\n",
    "\n",
    "    Raises:\n",
    "        AttributeError: The ``Raises`` section is a list of all exceptions\n",
    "            that are relevant to the interface.\n",
    "        ValueError: If `param2` is equal to `param1`.\n",
    "\n",
    "\n",
    "    .. _PEP 484:\n",
    "       https://www.python.org/dev/peps/pep-0484/\n",
    "\n",
    "    \"\"\"\n",
    "    if param1 == param2:\n",
    "        raise ValueError('param1 may not be equal to param2')\n",
    "    return True\n"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python [Root]",
   "language": "python",
   "name": "Python [Root]"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
